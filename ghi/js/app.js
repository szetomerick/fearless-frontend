window.addEventListener('DOMContentLoaded', async () => {

function createCard(name, description, pictureUrl, dateStarts, dateEnds, location) {
    return `
    <div class="card">
        <div class ="shadow p-1 mb-10 bg-body rounded">
        <div class="invisible">...</div>
        <div class="opacity-50">...
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
                <small class="text-dash-muted">${dateStarts} - ${dateEnds}</small>
                </div>
            </div>
            </div>
        </div>
    </div>
      `;
    }

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        alert("reponse not okay")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const starts = new Date(details.conference.starts);
            const dateStarts = starts.toLocaleDateString();
            const ends = new Date(details.conference.ends);
            const dateEnds = ends.toLocaleDateString();
            const location = details.conference.location.name;

            const html = createCard(title, description, pictureUrl, dateStarts, dateEnds, location);
            const column = document.querySelector('.card-group');
            column.innerHTML += html;
          }
        }

      }
    } catch (error) {
      // Figure out what to do if an error is raised
      console.error("error::", error);
      alert("error")
    }

  });